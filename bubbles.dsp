import("stdfaust.lib");
//TODO: fix "gost bubble" problem
//TODO: make bubblepattern a dirac, perhaps using sAH and delay?
f_min = vslider("[0]f min",100,100,2000,1); // original range: 1-3khz
f_max = vslider("[1]f max",3000,2000,3000,1);
gain  = vslider("[2]gain",0.2,0,1,0.01);
rand  = checkbox("size rand");//TODO: when ON disable the size knob
min_distance = 1;//s 
max_distance = 2;//s
noisepos = ((no.noise + 1)/2);
spread (value, minimum, maximum) = value*(maximum - minimum) + minimum;

bubble(seed) = os.osc(exp_env*freq) * lin_env*gain
    with{
        depth = 1; // vslider("depth[style:knob]",0.9,0.1,0.9,0.01); //TODO: remove in favour of only using the size (?)
        sizeslider = vslider("size [style:knob] b %seed",0.5,0.1,0.9,0.01);
        size  = ba.if(rand,noisepos : ba.sAndH(1-rand),sizeslider);
        freq = spread(1-size, f_min, f_max);

        //TODO: make the size actually dictate the lenght of the sample and not only the release
        release  = 0.2 + (.8 * size); // 0.1 + ... bw 0.2 and 0.7 
        
        pwm = en.asr(0,1,release,bubblePattern(20,0.01,seed/n_max_bubbles));
        exp_env = en.are(release,   .001, pwm); // original: 0.1, 0.001, pwm
        lin_env = en.ar (release/10, .08, pwm); // original: 0.01,0.080, pwm
    };

n_max_bubbles = 16;
n_active = vslider("[3]# active",1,0,n_max_bubbles,1);
odd  = hgroup("bubbles even", par(i,n_max_bubbles,bubble(i*3)*(i < n_active))) :> +;
even = hgroup("bubbles odd" , par(i,n_max_bubbles,bubble((i+1)*2)*(i < n_active))) :> +;

//process = vgroup("bubbles",(odd))<:_,_;
process = bubble(3)<:_,_;

//BUBBLEPATTERN: if bang_random_primed is on, then we have `prob_shoot` of shooting
//@seed between 0 and 1: modifies +-50% the clock of the generation
bubblePattern(freq,prob_shoot,seed) = en.ar(0,0.05,bang)
with{
    half_clock = freq * 0.5;
    freq_plus_minus_50_perc = half_clock + half_clock*seed;
    bang_random = no.lfnoise0(freq_plus_minus_50_perc)*200 : int;
    bang_random_primed = par(i,8,
                            ba.if((bang_random == ba.take(i+1,(29,37,47,67,89,113,157,197))),1,0)
                            ) :> _;
    bang = ba.if(noisepos < prob_shoot, bang_random_primed , 0) ;// i suspect that the if evaluation is not fast enought in order to have only one dirac, instead it creates longer portion of noise. therefore i added an ar                            
};
