# Bubbles in Faust!

A Faust implementation of the bubble stream physical model described by Andy Farnell in [Designing Sound](https://mitpress.mit.edu/9780262014410/)
